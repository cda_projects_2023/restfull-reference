package fr.afpa.restapi.controller;

import fr.afpa.restapi.entity.Address;
import fr.afpa.restapi.entity.Person;
import fr.afpa.restapi.service.implementation.AddressIMP;
import fr.afpa.restapi.service.implementation.PersonServiceIMP;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@Slf4j
@RestController
@RequestMapping("/person")
public class PersonController {

    private final PersonServiceIMP personServiceIMP;
    private final AddressIMP addressIMP;

    @PostMapping
    public ResponseEntity<Void> savePerson(@RequestBody Person person){
        if (person.getAddress().getIdAdress() == null){
            addressIMP.save(person.getAddress());
        }

        personServiceIMP.save(person);
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<List<Person>> findAll(){
        log.info("findall");
        List<Person> persons = personServiceIMP.findAll();
        if (persons != null){
            return new ResponseEntity<>(persons, HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Person> findById(@PathVariable("id") Long id){
        log.info("find by id");
        Person person = personServiceIMP.findById(id);
        if (person != null){
            return new ResponseEntity<>(person, HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

    }

    @PutMapping
    public ResponseEntity<Void> update(@RequestBody Person person){
        log.info("update");

        Person personToUpdate = personServiceIMP.findById(person.getIdPerson());
        if (personToUpdate != null) {
            personToUpdate.setName(person.getName());
            personToUpdate.setAge(person.getAge());

            personServiceIMP.update(personToUpdate);
            log.info("updated");
        }
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id){
        log.info("delete");

        Person person = personServiceIMP.findById(id);
        personServiceIMP.delete(person);

        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/deleteAddress/{id}")
    public ResponseEntity<Void> deleteAddress(@PathVariable("id") Long id){
        log.info("delete address");
        Address address = addressIMP.findById(id);
        List<Person> persons = personServiceIMP.findAll();

        persons.forEach(person -> {
            if (person.getAddress().getIdAdress().equals(address.getIdAdress())){
                address.getPersons().forEach(personServiceIMP::delete);
            }
        });

        addressIMP.delete(address);
        return ResponseEntity.noContent().build();
    }
}