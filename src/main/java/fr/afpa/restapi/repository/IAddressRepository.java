package fr.afpa.restapi.repository;

import fr.afpa.restapi.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IAddressRepository extends JpaRepository<Address, Long> {
}
