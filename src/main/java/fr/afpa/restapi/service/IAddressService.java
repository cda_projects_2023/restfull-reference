package fr.afpa.restapi.service;

import fr.afpa.restapi.entity.Address;

import java.util.List;

public interface IAddressService {
    void save(Address address);
    List<Address> findAll();
    Address findById(Long id);
    void update(Address address);
    void delete(Address address);

}
