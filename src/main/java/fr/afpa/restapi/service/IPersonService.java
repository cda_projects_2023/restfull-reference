package fr.afpa.restapi.service;

import fr.afpa.restapi.entity.Person;

import java.util.List;

public interface IPersonService {
    void save(Person person);
    List<Person> findAll();
    Person findById(Long id);
    void update(Person person);
    void delete(Person person);
}
